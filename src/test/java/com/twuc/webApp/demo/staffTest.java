package com.twuc.webApp.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.cache.Cache;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest(showSql = false)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class staffTest {

    @Autowired
    StaffRepository repo;

    @Autowired
    EntityManager em;

    @Test
    void should_create_entity() {
        Staff saveStaff = repo.save(new Staff(1l, "Shuning", "Li"));
        repo.flush();
        assertNotNull(saveStaff);
    }

    @Test
    void should_throw_error_when_firstNameOrLastNameIsNull() {
        assertThrows(DataIntegrityViolationException.class, () -> {
           repo.save(new Staff(1l, null, null));
           repo.flush();
        });
    }

    @Test
    void should_throw_error_when_firstNameOrLastNameOutOfLength() {
        assertThrows(DataIntegrityViolationException.class, () -> {
            repo.save(new Staff(1l, "11111111222323232321111111",  "333333"));
            repo.flush();
        });
    }
}
