package com.twuc.webApp.demo;

import javax.persistence.*;

@Entity
public class Staff {
    @Id
    private long id;

    @Embedded
    private Name name;

    public Staff() {
    }

    public Staff(long id, String firstName, String lastName) {
        this.id = id;
        this.name = new Name(firstName, lastName);
    }

    public long getId() {
        return id;
    }
}
